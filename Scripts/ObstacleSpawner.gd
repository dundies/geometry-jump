extends Node2D

const scnObstacle = preload("res://Scenes/Obstacle.tscn");
onready var player = Utils.get_main_node().get_node("Player");
onready var camera = Utils.get_main_node().get_node("Camera");

const AMOUNT_TO_FILL_VIEW = 4;

var INITIAL_PLAYER_OBSTACLE_SEPARATION_WIDTH = 500;
const PLAYER_OBSTACLE_SEPARATION_WIDTH = 300;

enum {
	DOWNWARD,
	UPWARD
}
const MOVEMENT_VELOCITY_MAP = [100, -100];

var previousTwoDirection = [null, null];

var screenSize = Vector2(0,0)
 
func _ready():
	INITIAL_PLAYER_OBSTACLE_SEPARATION_WIDTH = get_viewport().get_visible_rect().size.x + 50 # Get Width
	screenSize.y = get_viewport().get_visible_rect().size.y # Get Height
	if player:
		player.currentState.connect("finished", self, "_onPlayerStateChanged", [], CONNECT_ONESHOT);
	pass

func _onPlayerStateChanged(stateName): #TODO refactor
	if stateName == "flying":
		generate();
	
func generate():
	for i in range (AMOUNT_TO_FILL_VIEW):
		if i == 0:
			moveAndSpawn(true);
		else:
			moveAndSpawn(false);

func spawn():
	randomize();
	var newObstacle = scnObstacle.instance();
	
	newObstacle.position = self.position;
	
	var movement = _getMovement();
	
	newObstacle.generate(movement);
	newObstacle.connect("destroyed", self, "moveAndSpawn", [false]);
	get_node("Container").add_child(newObstacle);

func _getMovement():
	var movement = randi()%(UPWARD + 1);
	
	if previousTwoDirection[0] == null && previousTwoDirection[1] == null :
		previousTwoDirection[0] = movement;
		previousTwoDirection[1] = movement;
	else:
		while movement == previousTwoDirection[0] &&  movement == previousTwoDirection[1] :
			movement = randi()%(UPWARD + 1);
			
	previousTwoDirection[0] = previousTwoDirection[1];
	previousTwoDirection[1] = movement;
	return movement;
	
func move(isInitial):
	var positionX = self.position.x;
	
	if camera && isInitial:
		positionX += camera.position.x;
	
	if isInitial:
		positionX += INITIAL_PLAYER_OBSTACLE_SEPARATION_WIDTH;
	else:
		positionX += PLAYER_OBSTACLE_SEPARATION_WIDTH;
		
	self.position = Vector2(positionX, 0); 

	
func moveAndSpawn(isInitial):
	move(isInitial);
	spawn();
#	print("moveAndSpawn" + str(self.position.x));


