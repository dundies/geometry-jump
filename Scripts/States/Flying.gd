extends "States.gd"

func enter():
	owner.get_node("AnimationPlayer").play("idle")
	

func update(delta):
	if Input.is_action_just_pressed("flap"):
		AudioPlayer.get_node("flap").play();
		owner.linear_velocity = Vector2(owner.linear_velocity.x, -owner.JUMP_FORCE);

func onBodyEntered(otherBody):
	if (otherBody.is_in_group(Game.GROUP_OBSTACLES) or otherBody.is_in_group(Game.GROUP_CEILING)):
		emit_signal("finished", "hit");
	elif (otherBody.is_in_group(Game.GROUP_FLOOR)):
		emit_signal("finished", "grounded");
