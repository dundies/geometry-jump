extends Node

const GROUP_PLAYER = "Player";
const GROUP_OBSTACLES = "Obstacles";
const GROUP_PASS_AREAS = "PassAreas";
const GROUP_BIRDS = "Birds";
const GROUP_CEILING = "Ceiling";
const GROUP_FLOOR = "Floor";

const STATE_IDLE = "idle";
const STATE_FLYING = "flying";
const STATE_HIT = "hit";
const STATE_GROUNDED = "grounded";

var currentScore = 0 setget _setScoreCurrent;

var bestScore = 0;

signal score_current_changed;
signal best_score_current_changed;

func _ready():
	pass

func _setScoreCurrent(newValue):
	AudioPlayer.get_node("score").play();
	currentScore = newValue;
	emit_signal("score_current_changed");
	print(currentScore);
	if currentScore > bestScore:
		bestScore = currentScore;
		emit_signal("best_score_current_changed");
