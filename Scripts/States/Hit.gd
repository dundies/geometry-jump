extends "States.gd"

func enter():
	AudioPlayer.get_node("hit").play();
	owner.gravity_scale = 30;
	owner.linear_velocity = Vector2(0, 0);
	owner.angular_velocity = 0;
	owner.get_node("AnimationPlayer").stop();

#func onBodyEntered(otherBody):
#	if (otherBody.is_in_group(Game.GROUP_FLOOR)):
#		emit_signal("finished", "grounded");
