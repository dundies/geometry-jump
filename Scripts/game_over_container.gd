extends Container


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var player = Utils.get_main_node().get_node("Player");

# Called when the node enters the scene tree for the first time.
func _ready():
	if player:
		player.connect("player_state_changed", self, "on_player_state_changed");
	pass

func on_player_state_changed(stateName):
	if stateName == Game.STATE_HIT || stateName == Game.STATE_GROUNDED:
		show();

	pass



