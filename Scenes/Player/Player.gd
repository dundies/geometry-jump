extends RigidBody2D

onready var stateMap = {
	Game.STATE_IDLE: $States/Idle,
	Game.STATE_FLYING: $States/Flying,
	Game.STATE_HIT:  $States/Hit,
	Game.STATE_GROUNDED:  $States/Grounded
}

var currentState;

signal player_state_changed;

const SPEED = 200;
const JUMP_FORCE = 350;

func _ready():
	for state in $States.get_children():
		state.connect("finished", self, "_changeState");

	self.connect("body_entered", self, "_onBodyEntered");
	add_to_group(Game.GROUP_PLAYER);
	currentState = stateMap["idle"];
	currentState.enter();

func _physics_process(delta):
	
	if Input.is_action_pressed("ui_right"):
			get_tree().reload_current_scene();

	var label = get_node("Label");
	label.set_text(str(Game.currentScore));
	currentState.update(delta);
	
func _changeState(stateName):
	emit_signal("player_state_changed", stateName);
	currentState.exit();
	currentState = stateMap[stateName]; 
	currentState.enter();
	
func _onBodyEntered(otherBody):
	if currentState.has_method("onBodyEntered"):
		currentState.onBodyEntered(otherBody);
